#include <Kokkos_Core.hpp>

#include <iostream>
#include <fstream>
#include <math.h>

#define RANDOM_MAX 0x7fffffff // 2^31 - 1

typedef Kokkos::TeamPolicy<> team_policy;
typedef team_policy::member_type team_member;

void die(const char* msg) {
  std::cout << "ERROR: " << msg << std::endl;
  exit(1);
}

KOKKOS_INLINE_FUNCTION int random(const int seed) {
  return (int)((1103515245U * ((unsigned)seed & 0x7fffffffU) + 12345U) &
               0x7fffffffU);
}

template <class C>
KOKKOS_INLINE_FUNCTION C sign(const C x) {
  return x < 0 ? -1 : (x == 0 ? : 1);
}

template <class C>
KOKKOS_INLINE_FUNCTION C square(const C x) {
  return x * x;
}

/////////////////////////////////////////
// VIX
/////////////////////////////////////////

struct test_one_target {
  const int rank, nrows, roi, ox,oy,oz;

  Kokkos::View<int*> r;
  Kokkos::View<int*> nvis;
  Kokkos::View<const uint16_t*> elevs;


  test_one_target(const int rank_, const int nrows_, const int roi_,
                  const int ox_, const int oy_, const int oz_,
                  Kokkos::View<int*> r_, Kokkos::View<int*> nvis_,
                  Kokkos::View<const uint16_t*> elevs_)
    : rank(rank_), nrows(nrows_), roi(roi_), ox(ox_),oy(oy_),oz(oz_),
      r(r_), nvis(nvis_), elevs(elevs_) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(int i) const {
    int tx, ty, tz;

    r(rank) = random(r(rank));
    tx = (int)((2*roi+0.99999f)*r(rank)/RANDOM_MAX) + (ox-roi);
    r(rank) = random(r(rank));
    ty = (int)((2*roi+0.99999f)*r(rank)/RANDOM_MAX) + (oy-roi);
    
    if (square(tx - ox) + square(ty - oy) > square(roi)) {
      tx = ox + (tx - ox)/3;
      ty = oy + (ty - oy)/3;
    }

    if (tx<0 || tx>=nrows || ty<0 || ty>=nrows) return;
    
    if (abs(ox - tx) <= 1 && abs(oy - ty) <= 1) {
      Kokkos::atomic_fetch_add(&nvis(rank), 1);
      return;
    }

    int dx = tx - ox, dy = ty - oy;
    int px, py, pz;  // Current point
    int inciny = abs(dx) < abs(dy);
    int sign;
    float slope, zslope;
    sign = (inciny*dy + (1-inciny)*dx) > 0 ? 1 : -1;
    slope = (float)(inciny*dx + (1-inciny)*dy) / (inciny*dy + (1-inciny)*dx);
    zslope = (float)(tz - oz) / (inciny ? dy : dx);
    const int limit = inciny ? dy : dx;
    int stride = 1;
    for (int i=sign; abs(i) < abs(limit); i += stride*sign, stride <<= 1) {
      int j = round(i * slope);
      px = ox + (inciny*j + (1-inciny)*i);
      py = oy + (inciny*i + (1-inciny)*j);
      pz = elevs(px*nrows + py);
      if (pz > oz + i*zslope) return;
    }
    Kokkos::atomic_fetch_add(&nvis(rank), 1);
  }
};

struct calc_vix {
  const int nrows, roi, oht, tht, ntests;
  Kokkos::View<const uint16_t*> elevs;
  Kokkos::View<uint8_t*> vix;

	Kokkos::View<int*> nvis, r;

  calc_vix(const int nrows_, Kokkos::View<const uint16_t*> elevs_,
           const int roi_, const int oht_, const int tht_,
           const int ntests_, Kokkos::View<uint8_t*> vix_)
    : nrows(nrows_), elevs(elevs_), roi(roi_), oht(oht_), tht(tht_),
      ntests(ntests_), vix(vix_),
			nvis("nvis across threads", square(nrows)),
			r("random values across threads", square(nrows)) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(const team_member& thread) const {
    const int tid = thread.league_rank();
    const int ox = tid / nrows;
    const int oy = tid % nrows;
    const int oz = elevs(ox*nrows + oy) + oht;

		nvis(thread.league_rank()) = 0;
    r(thread.league_rank()) = tid; // Used for random selection
    Kokkos::parallel_for(Kokkos::TeamThreadRange(thread, ntests),
        test_one_target(thread.league_rank(), nrows, roi, ox,oy,oz, r, nvis, elevs));

    float v = (float)nvis(thread.league_rank()) / ntests;
    vix(ox*nrows + oy) = (uint8_t)std::min(255, (int)(v * 255.999f));
  }
};

/////////////////////////////////////////
// FINDMAX
/////////////////////////////////////////

struct find_max {
  const int nrows, nblocks, nwantedperblock;
  const float blocksize;
  Kokkos::View<uint8_t*> vix;
  Kokkos::View<int*> obs;

  find_max(const int nrows_, Kokkos::View<uint8_t*> vix_,
           const float blocksize_, const int nblocks_,
           const int nwantedperblock_, Kokkos::View<int*> obs_)
    : nrows(nrows_), vix(vix_), blocksize(blocksize_), nblocks(nblocks_),
      nwantedperblock(nwantedperblock_), obs(obs_) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(int i) const {
    const int bx = i/nblocks, by = i%nblocks;

    const int xmin = (int)(blocksize * bx);
    const int xmax = std::min((int)(blocksize * (bx + 1)), nrows);
    const int ymin = (int)(blocksize * by);
    const int ymax = std::min((int)(blocksize * (by + 1)), nrows);
    const int width = ymax - ymin;
    const int npoints = (xmax - xmin) * width;
    for (int n=0; n < nwantedperblock; n++) {
      int p1x = xmin;
      int p1y = ymin;
      uint8_t v1 = vix(p1x*nrows + p1y);
      int h1 = p1x * (p1x + p1y) * 010101010101;
      for (int j = 1; j < npoints; j++) {
        int p2x = xmin + j/width;
        int p2y = ymin + j%width;
        uint8_t v2 = vix(p2x*nrows + p2y);
        int h2 = p2x * (p2x + p2y) * 010101010101;
        if (v1 < v2 || (v1 == v2 && h1 < h2)) {
          p1x = p2x;
          p1y = p2y;
          v1 = v2;
          h1 = h2;
        }
      }
      obs(i*nwantedperblock + n) = p1x*nrows + p1y;
      vix(p1x*nrows + p1y) = 0;
    }
  }
};

/////////////////////////////////////////
// VIEWSHED
/////////////////////////////////////////

KOKKOS_INLINE_FUNCTION
void set_vis(const int nwpr, const int row, const int col,
             const int offset, Kokkos::View<uint64_t*> shed) {
  Kokkos::atomic_fetch_or(
    &shed(offset + (row*nwpr + col/64)),
    1ULL << (63 - col%64)
  );
}

struct calc_sheds {
  const int nrows, roi, oht, tht, nsheds;
  Kokkos::View<const uint16_t*> elevs;
  Kokkos::View<const int*> obs;
  Kokkos::View<uint64_t*> sheds;

  calc_sheds(const int nrows_, Kokkos::View<const uint16_t*> elevs_,
             const int roi_, const int oht_, const int tht_,
             const int nsheds_, Kokkos::View<const int*> obs_,
             Kokkos::View<uint64_t*> sheds_)
    : nrows(nrows_), elevs(elevs_), roi(roi_), oht(oht_), tht(tht_),
      nsheds(nsheds_), obs(obs_), sheds(sheds_) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(int i) const {
    const int observer[2] = {obs(i)/nrows, obs(i)%nrows};
    const int nr = 2*roi + 1;
    const int nwpr = (nr + 63) / 64;
    const int offset = i * nr * nwpr;

    int delta[2], target[2], p[2];
    int sig, pelev;
    float horizon_slope, slope, horizon_alt;
    int observer_alt;
    int inciny;

    const int xmin = observer[0] - roi;
    const int ymin = observer[1] - roi;
    const int xmax = observer[0] + roi;
    const int ymax = observer[1] + roi;
    const int xwidth = 2*roi + 1;
    const int ywidth = 2*roi + 1;
    const int perimeter = 8*roi + 4;

    set_vis(nwpr, roi, roi, offset, sheds);

    observer_alt = elevs(observer[0]*nrows + observer[1]) + oht;
    for (int ip = 0; ip < perimeter; ip++) {
      if (ip < xwidth) {
        target[0] = xmin + ip;
        target[1] = ymin;
      } else if (ip < 2 * xwidth) {
        target[0] = 1 + xmin - xwidth + ip;
        target[1] = ymax;
      } else if (ip < 2 * xwidth + ywidth) {
        target[0] = xmin;
        target[1] = 1 + ymin - 2 * xwidth + ip;
      } else {
        target[0] = xmax;
        target[1] = ymin - 2 * xwidth - ywidth + ip;
      }

      // don't clip
      // if (target[0] < 0) target[0] = 0;
      // if (target[0] >= nrows) target[0] = nrows - 1;
      // if (target[1] < 0) target[1] = 0;
      // if (target[1] >= nrows) target[1] = nrows - 1;

      // This occurs only when observer is on the edge of the region.
      if (observer[0] == target[0] && observer[1] == target[1]) continue;

      // Run a line of sight out from obs to target.
      delta[0] = target[0] - observer[0];
      delta[1] = target[1] - observer[1];
      inciny = (abs(delta[0]) < abs(delta[1]));  // outer parens reqd?

      // Step along the coord (X or Y) that varies the most from the observer to
      // the target.  Inciny says which coord that is.  Slope is how fast the
      // other coord varies.
      slope = (float)delta[1 - inciny] / (float)delta[inciny];
      sig = (delta[inciny] > 0 ? 1 : -1);
      horizon_slope = -99999.f;  // Slope (in vertical plane) to horizon so far.

      // i=0 would be the observer, which is always visible.
      for (int i = sig; i != delta[inciny] + sig; i += sig) {
        p[inciny] = observer[inciny] + i;
        p[1 - inciny] = observer[1 - inciny] + (int)round(i * slope);

        // Have we reached the edge of the area?
        if (p[0] < 0 || p[0] >= nrows || p[1] < 0 || p[1] >= nrows) break;
        if ((square(p[0] - observer[0]) + square(p[1] - observer[1]) > square(roi))) break;

        pelev = elevs[p[0] * nrows + p[1]];
        // Slope from the observer, incl the observer_ht, to this point, at ground
        // level.  The slope is projected into the plane XZ or YZ, depending on
        // whether X or Y is varying faster, and thus being iterated thru.
        float s = (float)(pelev - observer_alt) / (float)abs(p[inciny] - observer[inciny]);
        if (horizon_slope < s) horizon_slope = s;
        horizon_alt = observer_alt + horizon_slope * abs(p[inciny] - observer[inciny]);
        if (pelev + tht >= horizon_alt)
          set_vis(nwpr, p[0] - observer[0] + roi, p[1] - observer[1] + roi, offset, sheds);
      }
    }
  }
};

/////////////////////////////////////////
// SITE
/////////////////////////////////////////

const int BIT_COUNT[256] = {
    0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
    4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};

KOKKOS_INLINE_FUNCTION
int popcll(const uint64_t i) {
  int sum = 0;
  const uint8_t *c = reinterpret_cast<const uint8_t*>(&i);
  for (int j = 0; j < 8; j++)
    sum += BIT_COUNT[c[j]];
  return sum;
}

KOKKOS_INLINE_FUNCTION
int is_obs_vis(const int cumnwpr, Kokkos::View<const uint64_t*> cumshed,
               const int observer[2]) {
  int i = observer[0]*cumnwpr*64 + observer[1];
  if ((cumshed(i/64) & 1ULL << (63 - i%64)) != 0)
    return 1;
  else
    return 0;
}

struct union_area {
  const int nrows, roi, intervis, nsheds, nr, nwpr,
        cumnwpr, nusedsheds, lastobsx, lastobsy;
  Kokkos::View<const uint64_t*> sheds, cumshed;
  Kokkos::View<const int*> obs, bids;
  Kokkos::View<const char*> usedq;
  Kokkos::View<int*> testshedarea;

  union_area(Kokkos::View<const int*> bids_, const int nrows_,
             const int roi_, const int nusedsheds_,
             const int intervis_, const int nsheds_,
             Kokkos::View<const uint64_t*> sheds_,
             const int nr_, const int nwpr_, const int cumnwpr_,
             const int lastobsx_, const int lastobsy_,
             Kokkos::View<const uint64_t*> cumshed_,
						 Kokkos::View<const int*> obs_,
             Kokkos::View<int*> testshedarea_)
    : bids(bids_), nusedsheds(nusedsheds_), nrows(nrows_),
      roi(roi_), intervis(intervis_), nsheds(nsheds_), sheds(sheds_),
      nr(nr_), nwpr(nwpr_), cumnwpr(cumnwpr_), lastobsx(lastobsx_),
      lastobsy(lastobsy_), cumshed(cumshed_), obs(obs_),
			testshedarea(testshedarea_) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(int id) const {
    const int bid = bids(id);

		int observer[2];
    observer[0] = obs(bid)/nrows;
    observer[1] = obs(bid)%nrows;

		int areas[nr] = {0};

    int visible;
    if (intervis && nusedsheds > 0)
      visible = is_obs_vis(cumnwpr, cumshed, observer);

    for (int row = 0; row < nr; row++) {
      if (!intervis || nusedsheds==0 || visible) { // calculate one row of extra area
        const int cumrow = observer[0] - roi + row;
        if (cumrow >= 0 && cumrow < nrows) {
          int firstword = (observer[1] - roi) / 64;
          int firstbit = (observer[1] - roi) % 64;
          if (firstbit < 0) {
            firstbit--;
            firstbit += 64;
          }
          int lastword = (observer[1] + roi) / 64;

          uint64_t prevvalue = 0ULL;
          uint64_t value, cumvalue, tempvalue;
          int sum = 0;

          for (int cumword = firstword; cumword <= lastword; cumword++) {
            if (cumword >= 0 && cumword < cumnwpr) {
              int word = cumword - firstword;
              if (cumword == 0 && word > 0) prevvalue = sheds(bid*nr*nwpr + row*nwpr + word - 1);
              if (word < nwpr)
                value = sheds(bid*nr*nwpr + row*nwpr + word);
              else
                value = 0ULL;
              cumvalue = cumshed(cumrow*cumnwpr + cumword);
              tempvalue = cumvalue;
              tempvalue |= value >> firstbit;
              if (firstbit != 0) tempvalue |= prevvalue << (64 - firstbit);
              tempvalue ^= cumvalue;
              sum += popcll(tempvalue);
              prevvalue = value;
            }

            areas[row] = sum;
          }
        }
      }
    }

    int sum = 0;
    for (int i = 0; i < nr; i++)
      sum += areas[i];
    testshedarea(bid) = sum;
  }
};

struct calc_extra_area {
  Kokkos::View<int*> testshedarea;
  Kokkos::View<char*> usedq;

  calc_extra_area(Kokkos::View<int*> testshedarea_, Kokkos::View<char*> usedq_)
    : testshedarea(testshedarea_), usedq(usedq_) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(int i, Kokkos::MaxLoc<int,int>::value_type& value) const {
    if (!usedq(i) && testshedarea(i) > value.val) {
      value.val = testshedarea(i);
      value.loc = i;
    }
  }
};

struct calc_union_one_row { Kokkos::View<uint64_t*> cumshed;
  Kokkos::View<const uint64_t*> shed;
  const int offset, nr, nwpr, cumnwpr, roi, obsr, firstword, firstbit;

  calc_union_one_row(Kokkos::View<uint64_t*> cumshed_,
                     Kokkos::View<const uint64_t*> shed_,
										 const int offset_, const int nr_,
                     const int nwpr_, const int cumnwpr_,
                     const int roi_, const int obsr_,
                     const int firstword_, const int firstbit_)
    : cumshed(cumshed_), shed(shed_), offset(offset_), nr(nr_),
      nwpr(nwpr_), cumnwpr(cumnwpr_), roi(roi_), obsr(obsr_),
      firstword(firstword_), firstbit(firstbit_) {}

  KOKKOS_INLINE_FUNCTION
  void operator()(int cumrow) const {
    int row = cumrow - obsr + roi;
    for (int word = 0; word < nwpr; word++) {
      int cumword = firstword + word;
      if (cumword >= 0 && cumword < cumnwpr) { // word inside terrain
        Kokkos::atomic_fetch_or(
          &cumshed(cumrow*cumnwpr + cumword),
          shed(offset + row*nwpr + word) >> firstbit
        );
      }

      if (firstbit != 0 && cumword + 1 >= 0 && cumword + 1 < cumnwpr) {
        Kokkos::atomic_fetch_or(
          &cumshed(cumrow*cumnwpr + cumword + 1),
          shed(offset + row*nwpr + word) << (64 - firstbit)
        );
      }
    }
  }
};

KOKKOS_INLINE_FUNCTION
void calc_union(const int nrows, const int roi,
                Kokkos::View<const int*> obs, const int loc,
                Kokkos::View<const uint64_t*> shed,
                Kokkos::View<uint64_t*> cumshed) {
  const int nr = 2*roi + 1;
  const int nwpr = (nr + 63) / 64;
  const int cumnwpr = (nrows + 63) / 64;

  const int obsr = obs(loc)/nrows, obsc = obs(loc)%nrows;

  int firstword = (obsc - roi) / 64;
  int firstbit = (obsc - roi) % 64;
  if (firstbit < 0) {
    firstbit--;
    firstbit += 64;
  }

  int num_itr = obsr - roi + nr;
  if (num_itr > nrows) num_itr = nrows;
  Kokkos::parallel_for(std::min(obsr - roi + nr, nrows),
    calc_union_one_row(cumshed, shed, loc*nr*nwpr, nr, nwpr, cumnwpr, roi, obsr, firstword, firstbit));
}

KOKKOS_INLINE_FUNCTION
void site_it(const int nrows, const int roi, const int intervis,
             const int nsheds, Kokkos::View<const int*> obs,
             Kokkos::View<const uint64_t*> sheds,
             Kokkos::View<char*>::HostMirror selected) {
  const int nr = 2*roi + 1;
  const int nwpr = (nr + 63) / 64;
  const int cumnwpr = (nrows + 63) / 64;

  Kokkos::View<int*> usedsheds("sheds used so far", nsheds);
  Kokkos::View<char*> usedq("whether a shed has been used", nsheds);
  Kokkos::View<int[200]> top100("top 100 observers and extra areas");
  Kokkos::View<int*> testshedarea("test shed area", nsheds);
  Kokkos::View<uint64_t*> cumshed("cumulative viewshed", nrows*cumnwpr);
  Kokkos::View<int*> updatelist("bids which need updating", 500000);

  Kokkos::parallel_for(nsheds, KOKKOS_LAMBDA (const int i) {
    usedq(i) = 0;
    testshedarea(i) = 0;
  });
  Kokkos::parallel_for(nrows*cumnwpr, KOKKOS_LAMBDA (const int i) {
    cumshed(i) = 0ULL;
  });

  int nusedsheds=0, lastobsx=0, lastobsy=0, cumarea=0;

  Kokkos::View<int> updatelistlen("length of the new update list");

  while (1) {
    Kokkos::parallel_for("clear updatelist", 500000, KOKKOS_LAMBDA (const int i) {
      updatelist(i) = 0;
    });

    updatelistlen() = 0;
    Kokkos::parallel_for("determine which bids to update", nsheds, KOKKOS_LAMBDA (const int bid) {
      if (!usedq(bid)) {
        if (nusedsheds == 0 || square(obs(bid)/nrows-lastobsx) + square(obs(bid)%nrows-lastobsy) <= square(2*roi)) {
          const int idx = Kokkos::atomic_fetch_add(&updatelistlen(), 1);
          updatelist(idx) = bid;
        }
      }
    });

    Kokkos::parallel_for(updatelistlen(),
        union_area(updatelist, nrows, roi, nusedsheds, intervis, nsheds,
                   sheds, nr, nwpr, cumnwpr, lastobsx, lastobsy, cumshed,
                   obs, testshedarea));

    Kokkos::MaxLoc<int,int>::value_type max_area;
    Kokkos::MaxLoc<int,int> area_reduced(max_area);
    Kokkos::parallel_reduce("find largest added area",
        nsheds, calc_extra_area(testshedarea, usedq), area_reduced);

    if (max_area.val == 0)
      break;

    calc_union(nrows, roi, obs, max_area.loc, sheds, cumshed);
    usedsheds(nusedsheds++) = max_area.loc;
    usedq(max_area.loc) = 1;
    lastobsx = obs(max_area.loc) / nrows;
    lastobsy = obs(max_area.loc) % nrows;

    cumarea += max_area.val;
    double areapercentage = 100.0 * cumarea / square(nrows);
    if (areapercentage > 95)
      break;
  }

	std::cout << " nusedsheds:" << nusedsheds << " coverage:" << 100.0*cumarea/square(nrows);

  Kokkos::deep_copy(selected, usedq); // copy selected observers to host memory
}

/////////////////////////////////////////
// main
/////////////////////////////////////////

int main(int argc, char* argv[]) {
  Kokkos::initialize(argc, argv);
  {
    if (argc != 10) {
      std::cout << "argc=" << argc << std::endl;
      die("SITE requires 9 arguments: nrows, roi, oht/tht, ntests, blocksize, nwanted, intervis, infile, outfile");
    }

    Kokkos::Timer timer, overall_timer;

    int nrows;
    int roi;
    int oht;
    int tht;
    int ntests;
    int blocksize0;
    float blocksize;
    int nwanted0;
    int nwanted;
    int nblocks;
    int nwantedperblock;
    int intervis;

    Kokkos::View<uint16_t*> elevs;
    Kokkos::View<uint8_t*> vix;
    Kokkos::View<int*> obs;
    Kokkos::View<uint64_t*> sheds;
    Kokkos::View<char*> selected;

    Kokkos::View<uint16_t*>::HostMirror h_elevs;
    Kokkos::View<int*>::HostMirror h_obs;
    Kokkos::View<char*>::HostMirror h_selected;

    nrows = atoi(argv[1]);
    roi = atoi(argv[2]);
    oht = tht = atoi(argv[3]);
    ntests = atoi(argv[4]);
    blocksize0 = atoi(argv[5]);
    nwanted0 = atoi(argv[6]);
    intervis = atoi(argv[7]);

    if (nrows <= 0 || nrows > 20000) die("Unreasonable value for nrows");
    if (roi < 1 || roi > 10000) die("Unreasonable value for roi.");
    if (tht < 0 || tht > 1000000) die("Unreasonable value for tht.");
    if (ntests < 1 || ntests > 1000) die("Unreasonable value for ntests.");
    if (blocksize0 < 10 || blocksize0 > 2000) die("Unreasonable value for blocksize0.");
    if (nwanted0 < 100 || nwanted0 > 2000000) die("Unreasonable value for nwanted0.");
    if (intervis != 0 && intervis != 1) die("Unreasonable value for intervis.");

    // Perturb blocksize so that the last block won't be really small.
    blocksize = (float)nrows / (int)((float)nrows / blocksize0 + 0.5f);  // floating poin
    nblocks = (int)(nrows / blocksize + 0.5f);                             // number of b
    nwantedperblock = (int)((float)nwanted0 / square(nblocks) + 0.99999f);  // number of
    nwanted = nwantedperblock * square(nblocks);                          // number of wa
    int lastsize = nrows - (int)(blocksize * (nblocks - 1));              // size of the
    if (square(lastsize) < nwantedperblock)                               // too small
      die("The last block is too small for nwantedperblock.");

    const int nr = 2*roi + 1;
    const int nwpr = (nr + 63) / 64;

    elevs = Kokkos::View<uint16_t*>("terrain elevation (input)", square(nrows));
    vix = Kokkos::View<uint8_t*>("visibility index * 256 (output)", square(nrows));
    obs = Kokkos::View<int*>("observers", nwanted);
    sheds = Kokkos::View<uint64_t*>("viewsheds", nwanted * nr * nwpr);
    selected = Kokkos::View<char*>("selected observers", nwanted);

    h_elevs = Kokkos::create_mirror_view(elevs);
    h_obs = Kokkos::create_mirror_view(obs);
    h_selected = Kokkos::create_mirror_view(selected);

    uint16_t input_elevs[square(nrows)];

    std::ifstream ifs(argv[8]);
    ifs.read((char*)input_elevs, square(nrows)*sizeof(uint16_t));
    if (ifs.fail())
      die("Input failed");
    ifs.close();

    for (int i=0; i<square(nrows); i++)
      h_elevs(i) = input_elevs[i];

    Kokkos::deep_copy(elevs, h_elevs); // copy input data to device

    const team_policy vix_policy(square(nrows), Kokkos::AUTO);

    std::cout << "input:" << timer.seconds();
    timer.reset();

    Kokkos::parallel_for(vix_policy, calc_vix(nrows, elevs, roi, oht, tht, ntests, vix));
    std::cout << " vix:" << timer.seconds();
    timer.reset();

    Kokkos::parallel_for(square(nblocks), find_max(nrows, vix, blocksize, nblocks, nwantedperblock, obs));
    std::cout << " findmax:" << timer.seconds();
    timer.reset();

    Kokkos::parallel_for(nwanted, calc_sheds(nrows, elevs, roi, oht, tht, nwanted, obs, sheds));
    std::cout << " viewshed:" << timer.seconds();
    timer.reset();

    site_it(nrows, roi, intervis, nwanted, obs, sheds, selected);
    std::cout << " site:" << timer.seconds();
    timer.reset();

    Kokkos::deep_copy(h_obs, obs);

    std::ofstream ofs(argv[9]);
    for (int i = 0; i < nwanted; i++)
      if (selected(i))
        ofs << h_obs(i)/nrows << ',' << h_obs(i)%nrows << '\n';
    ofs.close();

    std::cout << " output:" << timer.seconds()
      << " total:" << overall_timer.seconds() << std::endl;
  }
  Kokkos::finalize();
  return 0;
}
